<p><a href="http://travis-ci.org/Shopify/liquid"><img src="https://api.travis-ci.org/Shopify/liquid.svg?branch=master" alt="Build Status" /></a> <a href="http://inch-ci.org/github/Shopify/liquid"><img src="http://inch-ci.org/github/Shopify/liquid.svg?branch=master" alt="Inline docs" /></a></p>
<h1 id="liquid-template-engine">Liquid template engine</h1>
<ul>
<li><a href="CONTRIBUTING.md">Contributing guidelines</a></li>
<li><a href="History.md">Version history</a></li>
<li><a href="http://docs.shopify.com/themes/liquid-basics">Liquid documentation from Shopify</a></li>
<li><a href="https://github.com/Shopify/liquid/wiki">Liquid Wiki at GitHub</a></li>
<li><a href="http://liquidmarkup.org/">Website</a></li>
</ul>
<h2 id="introduction">Introduction</h2>
<p>Liquid is a template engine which was written with very specific requirements:</p>
<ul>
<li>It has to have beautiful and simple markup. Template engines which don’t produce good looking markup are no fun to use.</li>
<li>It needs to be non evaling and secure. Liquid templates are made so that users can edit them. You don’t want to run code on your server which your users wrote.</li>
<li>It has to be stateless. Compile and render steps have to be separate so that the expensive parsing and compiling can be done once and later on you can just render it passing in a hash with local variables and objects.</li>
</ul>
<h2 id="why-you-should-use-liquid">Why you should use Liquid</h2>
<ul>
<li>You want to allow your users to edit the appearance of your application but don’t want them to run <strong>insecure code on your server</strong>.</li>
<li>You want to render templates directly from the database.</li>
<li>You like smarty (PHP) style template engines.</li>
<li>You need a template engine which does HTML just as well as emails.</li>
<li>You don’t like the markup of your current templating engine.</li>
</ul>
<h2 id="what-does-it-look-like">What does it look like?</h2>
<div class="sourceCode" id="cb1"><pre class="sourceCode html"><code class="sourceCode html"><a class="sourceLine" id="cb1-1" title="1"><span class="kw">&lt;ul</span><span class="ot"> id=</span><span class="st">&quot;products&quot;</span><span class="kw">&gt;</span></a>
<a class="sourceLine" id="cb1-2" title="2">  {% for product in products %}</a>
<a class="sourceLine" id="cb1-3" title="3">    <span class="kw">&lt;li&gt;</span></a>
<a class="sourceLine" id="cb1-4" title="4">      <span class="kw">&lt;h2&gt;</span>{{ product.name }}<span class="kw">&lt;/h2&gt;</span></a>
<a class="sourceLine" id="cb1-5" title="5">      Only {{ product.price | price }}</a>
<a class="sourceLine" id="cb1-6" title="6"></a>
<a class="sourceLine" id="cb1-7" title="7">      {{ product.description | prettyprint | paragraph }}</a>
<a class="sourceLine" id="cb1-8" title="8">    <span class="kw">&lt;/li&gt;</span></a>
<a class="sourceLine" id="cb1-9" title="9">  {% endfor %}</a>
<a class="sourceLine" id="cb1-10" title="10"><span class="kw">&lt;/ul&gt;</span></a></code></pre></div>
<h2 id="how-to-use-liquid">How to use Liquid</h2>
<p>Liquid supports a very simple API based around the Liquid::Template class. For standard use you can just pass it the content of a file and call render with a parameters hash.</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb2-1" title="1"><span class="ot">@template</span> = <span class="dt">Liquid</span>::<span class="dt">Template</span>.parse(<span class="st">&quot;hi {{name}}&quot;</span>) <span class="co"># Parses and compiles the template</span></a>
<a class="sourceLine" id="cb2-2" title="2"><span class="ot">@template</span>.render(<span class="st">&#39;name&#39;</span> =&gt; <span class="st">&#39;tobi&#39;</span>)                <span class="co"># =&gt; &quot;hi tobi&quot;</span></a></code></pre></div>
<h3 id="error-modes">Error Modes</h3>
<p>Setting the error mode of Liquid lets you specify how strictly you want your templates to be interpreted. Normally the parser is very lax and will accept almost anything without error. Unfortunately this can make it very hard to debug and can lead to unexpected behaviour.</p>
<p>Liquid also comes with a stricter parser that can be used when editing templates to give better error messages when templates are invalid. You can enable this new parser like this:</p>
<div class="sourceCode" id="cb3"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb3-1" title="1"><span class="dt">Liquid</span>::<span class="dt">Template</span>.error_mode = <span class="st">:strict</span> <span class="co"># Raises a SyntaxError when invalid syntax is used</span></a>
<a class="sourceLine" id="cb3-2" title="2"><span class="dt">Liquid</span>::<span class="dt">Template</span>.error_mode = <span class="st">:warn</span> <span class="co"># Adds errors to template.errors but continues as normal</span></a>
<a class="sourceLine" id="cb3-3" title="3"><span class="dt">Liquid</span>::<span class="dt">Template</span>.error_mode = <span class="st">:lax</span> <span class="co"># The default mode, accepts almost anything.</span></a></code></pre></div>
<p>If you want to set the error mode only on specific templates you can pass <code>:error_mode</code> as an option to <code>parse</code>:</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb4-1" title="1"><span class="dt">Liquid</span>::<span class="dt">Template</span>.parse(source, <span class="st">:error_mode</span> =&gt; <span class="st">:strict</span>)</a></code></pre></div>
<p>This is useful for doing things like enabling strict mode only in the theme editor.</p>
<p>It is recommended that you enable <code>:strict</code> or <code>:warn</code> mode on new apps to stop invalid templates from being created. It is also recommended that you use it in the template editors of existing apps to give editors better error messages.</p>
<h3 id="undefined-variables-and-filters">Undefined variables and filters</h3>
<p>By default, the renderer doesn’t raise or in any other way notify you if some variables or filters are missing, i.e. not passed to the <code>render</code> method. You can improve this situation by passing <code>strict_variables: true</code> and/or <code>strict_filters: true</code> options to the <code>render</code> method. When one of these options is set to true, all errors about undefined variables and undefined filters will be stored in <code>errors</code> array of a <code>Liquid::Template</code> instance. Here are some examples:</p>
<div class="sourceCode" id="cb5"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb5-1" title="1">template = <span class="dt">Liquid</span>::<span class="dt">Template</span>.parse(<span class="st">&quot;{{x}} {{y}} {{z.a}} {{z.b}}&quot;</span>)</a>
<a class="sourceLine" id="cb5-2" title="2">template.render({ <span class="ch">&#39;x&#39;</span> =&gt; <span class="dv">1</span>, <span class="ch">&#39;z&#39;</span> =&gt; { <span class="ch">&#39;a&#39;</span> =&gt; <span class="dv">2</span> } }, { <span class="st">strict_variables: </span><span class="dv">true</span> })</a>
<a class="sourceLine" id="cb5-3" title="3"><span class="co">#=&gt; &#39;1  2 &#39; # when a variable is undefined, it&#39;s rendered as nil</span></a>
<a class="sourceLine" id="cb5-4" title="4">template.errors</a>
<a class="sourceLine" id="cb5-5" title="5"><span class="co">#=&gt; [#&lt;Liquid::UndefinedVariable: Liquid error: undefined variable y&gt;, #&lt;Liquid::UndefinedVariable: Liquid error: undefined variable b&gt;]</span></a></code></pre></div>
<div class="sourceCode" id="cb6"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb6-1" title="1">template = <span class="dt">Liquid</span>::<span class="dt">Template</span>.parse(<span class="st">&quot;{{x | filter1 | upcase}}&quot;</span>)</a>
<a class="sourceLine" id="cb6-2" title="2">template.render({ <span class="ch">&#39;x&#39;</span> =&gt; <span class="st">&#39;foo&#39;</span> }, { <span class="st">strict_filters: </span><span class="dv">true</span> })</a>
<a class="sourceLine" id="cb6-3" title="3"><span class="co">#=&gt; &#39;&#39; # when at least one filter in the filter chain is undefined, a whole expression is rendered as nil</span></a>
<a class="sourceLine" id="cb6-4" title="4">template.errors</a>
<a class="sourceLine" id="cb6-5" title="5"><span class="co">#=&gt; [#&lt;Liquid::UndefinedFilter: Liquid error: undefined filter filter1&gt;]</span></a></code></pre></div>
<p>If you want to raise on a first exception instead of pushing all of them in <code>errors</code>, you can use <code>render!</code> method:</p>
<div class="sourceCode" id="cb7"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb7-1" title="1">template = <span class="dt">Liquid</span>::<span class="dt">Template</span>.parse(<span class="st">&quot;{{x}} {{y}}&quot;</span>)</a>
<a class="sourceLine" id="cb7-2" title="2">template.render!({ <span class="ch">&#39;x&#39;</span> =&gt; <span class="dv">1</span>}, { <span class="st">strict_variables: </span><span class="dv">true</span> })</a>
<a class="sourceLine" id="cb7-3" title="3"><span class="co">#=&gt; Liquid::UndefinedVariable: Liquid error: undefined variable y</span></a></code></pre></div>
