<h1 id="jekyll-avatar">Jekyll Avatar</h1>
<p><em>A Jekyll plugin for rendering GitHub avatars</em></p>
<p><a href="https://travis-ci.org/benbalter/jekyll-avatar"><img src="https://travis-ci.org/benbalter/jekyll-avatar.svg" alt="Build Status" /></a></p>
<p>Jekyll Avatar makes it easy to add GitHub avatars to your Jekyll site by specifying a username. If performance is a concern, Jekyll Avatar is deeply integrated with the GitHub avatar API, ensuring avatars are cached and load in parallel. It even automatically upgrades users to Retina images, when supported.</p>
<h2 id="installation">Installation</h2>
<p>Add the following to your site’s <code>Gemfile</code>:</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb1-1" title="1">gem <span class="st">&#39;jekyll-avatar&#39;</span></a></code></pre></div>
<p>And add the following to your site’s <code>_config.yml</code> file:</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode yaml"><code class="sourceCode yaml"><a class="sourceLine" id="cb2-1" title="1"><span class="fu">gems:</span></a>
<a class="sourceLine" id="cb2-2" title="2">  <span class="kw">-</span> jekyll-avatar</a></code></pre></div>
<h2 id="usage">Usage</h2>
<p>Simply add the following, anywhere you’d like a user’s avatar to appear:</p>
<pre><code>{% avatar [USERNAME] %}</code></pre>
<p>With <code>[USERNAME]</code> being the user’s GitHub username:</p>
<pre><code>{% avatar hubot %}</code></pre>
<p>That will output:</p>
<div class="sourceCode" id="cb5"><pre class="sourceCode html"><code class="sourceCode html"><a class="sourceLine" id="cb5-1" title="1"><span class="kw">&lt;img</span><span class="ot"> class=</span><span class="st">&quot;avatar avatar-small&quot;</span><span class="ot"> src=</span><span class="st">&quot;https://avatars3.githubusercontent.com/hubot?v=3</span><span class="dv">&amp;amp;</span><span class="st">s=40&quot;</span><span class="ot"> alt=</span><span class="st">&quot;hubot&quot;</span><span class="ot"> srcset=</span><span class="st">&quot;https://avatars3.githubusercontent.com/hubot?v=3</span><span class="dv">&amp;amp;</span><span class="st">s=40 1x, https://avatars3.githubusercontent.com/hubot?v=3</span><span class="dv">&amp;amp;</span><span class="st">s=80 2x, https://avatars3.githubusercontent.com/hubot?v=3</span><span class="dv">&amp;amp;</span><span class="st">s=120 3x, https://avatars3.githubusercontent.com/hubot?v=3</span><span class="dv">&amp;amp;</span><span class="st">s=160 4x&quot;</span><span class="ot"> width=</span><span class="st">&quot;40&quot;</span><span class="ot"> height=</span><span class="st">&quot;40&quot;</span> <span class="kw">/&gt;</span></a></code></pre></div>
<h3 id="customizing">Customizing</h3>
<p>You can customize the size of the resulting avatar by passing the size argument:</p>
<pre><code>{% avatar hubot size=50 %}</code></pre>
<p>That will output:</p>
<div class="sourceCode" id="cb7"><pre class="sourceCode html"><code class="sourceCode html"><a class="sourceLine" id="cb7-1" title="1"><span class="kw">&lt;img</span><span class="ot"> class=</span><span class="st">&quot;avatar&quot;</span><span class="ot"> src=</span><span class="st">&quot;https://avatars3.githubusercontent.com/hubot?v=3</span><span class="dv">&amp;amp;</span><span class="st">s=50&quot;</span><span class="ot"> alt=</span><span class="st">&quot;hubot&quot;</span><span class="ot"> srcset=</span><span class="st">&quot;https://avatars3.githubusercontent.com/hubot?v=3</span><span class="dv">&amp;amp;</span><span class="st">s=50 1x, https://avatars3.githubusercontent.com/hubot?v=3</span><span class="dv">&amp;amp;</span><span class="st">s=100 2x, https://avatars3.githubusercontent.com/hubot?v=3</span><span class="dv">&amp;amp;</span><span class="st">s=150 3x, https://avatars3.githubusercontent.com/hubot?v=3</span><span class="dv">&amp;amp;</span><span class="st">s=200 4x&quot;</span><span class="ot"> width=</span><span class="st">&quot;50&quot;</span><span class="ot"> height=</span><span class="st">&quot;50&quot;</span> <span class="kw">/&gt;</span></a></code></pre></div>
<h3 id="passing-the-username-as-variable">Passing the username as variable</h3>
<p>You can also pass the username as a variable, like this:</p>
<pre><code>{% assign username=&quot;hubot&quot; %}
{% avatar {{ username }} %}</code></pre>
<p>Or, if the variable is someplace a bit more complex, like a loop:</p>
<pre><code>{% assign employees = &quot;alice|bob&quot; | split:&quot;|&quot; %}
{% for employee in employees %}
  {% avatar user=employee %}
{% endfor %}</code></pre>
<h3 id="lazy-loading-images">Lazy loading images</h3>
<p>For pages showing a large number of avatars, you may want to load the images lazily.</p>
<pre class="liquid"><code>{% avatar hubot lazy=true %}</code></pre>
<p>This will set the <code>data-src</code> and <code>data-srcset</code> attributes on the <code>&lt;img&gt;</code> tag, which is compatible with many lazy load JavaScript plugins, such as:</p>
<ul>
<li>https://www.andreaverlicchi.eu/lazyload/</li>
<li>https://appelsiini.net/projects/lazyload/</li>
</ul>
<h3 id="using-with-github-enterprise">Using with GitHub Enterprise</h3>
<p>To use Jekyll Avatars with GitHub Enterprise, you must set the <code>PAGES_AVATARS_URL</code> environmental variable.</p>
<p>This should be the full URL to the avatars subdomain or subpath. For example:</p>
<ul>
<li>With subdomain isolation: <code>PAGES_AVATARS_URL="https://avatars.github.example.com"</code></li>
<li>Without subdomain isolation: <code>PAGES_AVATARS_URL="https://github.example.com/avatars"</code></li>
</ul>
