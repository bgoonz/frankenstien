<h1 id="htmlpipeline-build-status">HTML::Pipeline <a href="https://travis-ci.org/jch/html-pipeline"><img src="https://travis-ci.org/jch/html-pipeline.svg?branch=master" alt="Build Status" /></a></h1>
<p>GitHub HTML processing filters and utilities. This module includes a small framework for defining DOM based content filters and applying them to user provided content. Read an introduction about this project in <a href="https://github.com/blog/1311-html-pipeline-chainable-content-filters">this blog post</a>.</p>
<ul>
<li><a href="#installation">Installation</a></li>
<li><a href="#usage">Usage</a>
<ul>
<li><a href="#examples">Examples</a></li>
</ul></li>
<li><a href="#filters">Filters</a></li>
<li><a href="#dependencies">Dependencies</a></li>
<li><a href="#documentation">Documentation</a></li>
<li><a href="#extending">Extending</a>
<ul>
<li><a href="#3rd-party-extensions">3rd Party Extensions</a></li>
</ul></li>
<li><a href="#instrumenting">Instrumenting</a></li>
<li><a href="#contributing">Contributing</a>
<ul>
<li><a href="#contributors">Contributors</a></li>
<li><a href="#releasing-a-new-version">Releasing A New Version</a></li>
</ul></li>
</ul>
<h2 id="installation">Installation</h2>
<p>Add this line to your application’s Gemfile:</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb1-1" title="1">gem <span class="st">&#39;html-pipeline&#39;</span></a></code></pre></div>
<p>And then execute:</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode sh"><code class="sourceCode bash"><a class="sourceLine" id="cb2-1" title="1">$ <span class="ex">bundle</span></a></code></pre></div>
<p>Or install it yourself as:</p>
<div class="sourceCode" id="cb3"><pre class="sourceCode sh"><code class="sourceCode bash"><a class="sourceLine" id="cb3-1" title="1">$ <span class="ex">gem</span> install html-pipeline</a></code></pre></div>
<h2 id="usage">Usage</h2>
<p>This library provides a handful of chainable HTML filters to transform user content into markup. A filter takes an HTML string or <code>Nokogiri::HTML::DocumentFragment</code>, optionally manipulates it, and then outputs the result.</p>
<p>For example, to transform Markdown source into Markdown HTML:</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb4-1" title="1">require <span class="st">&#39;html/pipeline&#39;</span></a>
<a class="sourceLine" id="cb4-2" title="2"></a>
<a class="sourceLine" id="cb4-3" title="3">filter = <span class="dt">HTML</span>::<span class="dt">Pipeline</span>::<span class="dt">MarkdownFilter</span>.new(<span class="st">&quot;Hi **world**!&quot;</span>)</a>
<a class="sourceLine" id="cb4-4" title="4">filter.call</a></code></pre></div>
<p>Filters can be combined into a pipeline which causes each filter to hand its output to the next filter’s input. So if you wanted to have content be filtered through Markdown and be syntax highlighted, you can create the following pipeline:</p>
<div class="sourceCode" id="cb5"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb5-1" title="1">pipeline = <span class="dt">HTML</span>::<span class="dt">Pipeline</span>.new [</a>
<a class="sourceLine" id="cb5-2" title="2">  <span class="dt">HTML</span>::<span class="dt">Pipeline</span>::<span class="dt">MarkdownFilter</span>,</a>
<a class="sourceLine" id="cb5-3" title="3">  <span class="dt">HTML</span>::<span class="dt">Pipeline</span>::<span class="dt">SyntaxHighlightFilter</span></a>
<a class="sourceLine" id="cb5-4" title="4">]</a>
<a class="sourceLine" id="cb5-5" title="5">result = pipeline.call &lt;&lt;-<span class="kw">CODE</span></a>
<a class="sourceLine" id="cb5-6" title="6"><span class="ot">This is *great*:</span></a>
<a class="sourceLine" id="cb5-7" title="7"></a>
<a class="sourceLine" id="cb5-8" title="8"><span class="ot">    some_code(:first)</span></a>
<a class="sourceLine" id="cb5-9" title="9"></a>
<a class="sourceLine" id="cb5-10" title="10"><span class="kw">CODE</span></a>
<a class="sourceLine" id="cb5-11" title="11">result[<span class="st">:output</span>].to_s</a></code></pre></div>
<p>Prints:</p>
<div class="sourceCode" id="cb6"><pre class="sourceCode html"><code class="sourceCode html"><a class="sourceLine" id="cb6-1" title="1"><span class="kw">&lt;p&gt;</span>This is <span class="kw">&lt;em&gt;</span>great<span class="kw">&lt;/em&gt;</span>:<span class="kw">&lt;/p&gt;</span></a>
<a class="sourceLine" id="cb6-2" title="2"></a>
<a class="sourceLine" id="cb6-3" title="3"><span class="kw">&lt;pre&gt;&lt;code&gt;</span>some_code(:first)</a>
<a class="sourceLine" id="cb6-4" title="4"><span class="kw">&lt;/code&gt;&lt;/pre&gt;</span></a></code></pre></div>
<p>To generate CSS for HTML formatted code, use the <a href="https://github.com/jneen/rouge#css-theme-options">Rouge CSS Theme</a> <code>#css</code> method. <code>rouge</code> is a dependency of the <code>SyntaxHighlightFilter</code>.</p>
<p>Some filters take an optional <strong>context</strong> and/or <strong>result</strong> hash. These are used to pass around arguments and metadata between filters in a pipeline. For example, if you don’t want to use GitHub formatted Markdown, you can pass an option in the context hash:</p>
<div class="sourceCode" id="cb7"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb7-1" title="1">filter = <span class="dt">HTML</span>::<span class="dt">Pipeline</span>::<span class="dt">MarkdownFilter</span>.new(<span class="st">&quot;Hi **world**!&quot;</span>, <span class="st">:gfm</span> =&gt; <span class="dv">false</span>)</a>
<a class="sourceLine" id="cb7-2" title="2">filter.call</a></code></pre></div>
<h3 id="examples">Examples</h3>
<p>We define different pipelines for different parts of our app. Here are a few paraphrased snippets to get you started:</p>
<div class="sourceCode" id="cb8"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb8-1" title="1"><span class="co"># The context hash is how you pass options between different filters.</span></a>
<a class="sourceLine" id="cb8-2" title="2"><span class="co"># See individual filter source for explanation of options.</span></a>
<a class="sourceLine" id="cb8-3" title="3">context = {</a>
<a class="sourceLine" id="cb8-4" title="4">  <span class="st">:asset_root</span> =&gt; <span class="st">&quot;http://your-domain.com/where/your/images/live/icons&quot;</span>,</a>
<a class="sourceLine" id="cb8-5" title="5">  <span class="st">:base_url</span>   =&gt; <span class="st">&quot;http://your-domain.com&quot;</span></a>
<a class="sourceLine" id="cb8-6" title="6">}</a>
<a class="sourceLine" id="cb8-7" title="7"></a>
<a class="sourceLine" id="cb8-8" title="8"><span class="co"># Pipeline providing sanitization and image hijacking but no mention</span></a>
<a class="sourceLine" id="cb8-9" title="9"><span class="co"># related features.</span></a>
<a class="sourceLine" id="cb8-10" title="10"><span class="dt">SimplePipeline</span> = <span class="dt">Pipeline</span>.new [</a>
<a class="sourceLine" id="cb8-11" title="11">  <span class="dt">SanitizationFilter</span>,</a>
<a class="sourceLine" id="cb8-12" title="12">  <span class="dt">TableOfContentsFilter</span>, <span class="co"># add &#39;name&#39; anchors to all headers and generate toc list</span></a>
<a class="sourceLine" id="cb8-13" title="13">  <span class="dt">CamoFilter</span>,</a>
<a class="sourceLine" id="cb8-14" title="14">  <span class="dt">ImageMaxWidthFilter</span>,</a>
<a class="sourceLine" id="cb8-15" title="15">  <span class="dt">SyntaxHighlightFilter</span>,</a>
<a class="sourceLine" id="cb8-16" title="16">  <span class="dt">EmojiFilter</span>,</a>
<a class="sourceLine" id="cb8-17" title="17">  <span class="dt">AutolinkFilter</span></a>
<a class="sourceLine" id="cb8-18" title="18">], context</a>
<a class="sourceLine" id="cb8-19" title="19"></a>
<a class="sourceLine" id="cb8-20" title="20"><span class="co"># Pipeline used for user provided content on the web</span></a>
<a class="sourceLine" id="cb8-21" title="21"><span class="dt">MarkdownPipeline</span> = <span class="dt">Pipeline</span>.new [</a>
<a class="sourceLine" id="cb8-22" title="22">  <span class="dt">MarkdownFilter</span>,</a>
<a class="sourceLine" id="cb8-23" title="23">  <span class="dt">SanitizationFilter</span>,</a>
<a class="sourceLine" id="cb8-24" title="24">  <span class="dt">CamoFilter</span>,</a>
<a class="sourceLine" id="cb8-25" title="25">  <span class="dt">ImageMaxWidthFilter</span>,</a>
<a class="sourceLine" id="cb8-26" title="26">  <span class="dt">HttpsFilter</span>,</a>
<a class="sourceLine" id="cb8-27" title="27">  <span class="dt">MentionFilter</span>,</a>
<a class="sourceLine" id="cb8-28" title="28">  <span class="dt">EmojiFilter</span>,</a>
<a class="sourceLine" id="cb8-29" title="29">  <span class="dt">SyntaxHighlightFilter</span></a>
<a class="sourceLine" id="cb8-30" title="30">], context.merge(<span class="st">:gfm</span> =&gt; <span class="dv">true</span>) <span class="co"># enable github formatted markdown</span></a>
<a class="sourceLine" id="cb8-31" title="31"></a>
<a class="sourceLine" id="cb8-32" title="32"></a>
<a class="sourceLine" id="cb8-33" title="33"><span class="co"># Define a pipeline based on another pipeline&#39;s filters</span></a>
<a class="sourceLine" id="cb8-34" title="34"><span class="dt">NonGFMMarkdownPipeline</span> = <span class="dt">Pipeline</span>.new(<span class="dt">MarkdownPipeline</span>.filters,</a>
<a class="sourceLine" id="cb8-35" title="35">  context.merge(<span class="st">:gfm</span> =&gt; <span class="dv">false</span>))</a>
<a class="sourceLine" id="cb8-36" title="36"></a>
<a class="sourceLine" id="cb8-37" title="37"><span class="co"># Pipelines aren&#39;t limited to the web. You can use them for email</span></a>
<a class="sourceLine" id="cb8-38" title="38"><span class="co"># processing also.</span></a>
<a class="sourceLine" id="cb8-39" title="39"><span class="dt">HtmlEmailPipeline</span> = <span class="dt">Pipeline</span>.new [</a>
<a class="sourceLine" id="cb8-40" title="40">  <span class="dt">PlainTextInputFilter</span>,</a>
<a class="sourceLine" id="cb8-41" title="41">  <span class="dt">ImageMaxWidthFilter</span></a>
<a class="sourceLine" id="cb8-42" title="42">], {}</a>
<a class="sourceLine" id="cb8-43" title="43"></a>
<a class="sourceLine" id="cb8-44" title="44"><span class="co"># Just emoji.</span></a>
<a class="sourceLine" id="cb8-45" title="45"><span class="dt">EmojiPipeline</span> = <span class="dt">Pipeline</span>.new [</a>
<a class="sourceLine" id="cb8-46" title="46">  <span class="dt">PlainTextInputFilter</span>,</a>
<a class="sourceLine" id="cb8-47" title="47">  <span class="dt">EmojiFilter</span></a>
<a class="sourceLine" id="cb8-48" title="48">], context</a></code></pre></div>
<h2 id="filters">Filters</h2>
<ul>
<li><code>MentionFilter</code> - replace <code>@user</code> mentions with links</li>
<li><code>AbsoluteSourceFilter</code> - replace relative image urls with fully qualified versions</li>
<li><code>AutolinkFilter</code> - auto_linking urls in HTML</li>
<li><code>CamoFilter</code> - replace http image urls with <a href="https://github.com/atmos/camo">camo-fied</a> https versions</li>
<li><code>EmailReplyFilter</code> - util filter for working with emails</li>
<li><code>EmojiFilter</code> - everyone loves <a href="http://www.emoji-cheat-sheet.com/">emoji</a>!</li>
<li><code>HttpsFilter</code> - HTML Filter for replacing http github urls with https versions.</li>
<li><code>ImageMaxWidthFilter</code> - link to full size image for large images</li>
<li><code>MarkdownFilter</code> - convert markdown to html</li>
<li><code>PlainTextInputFilter</code> - html escape text and wrap the result in a div</li>
<li><code>SanitizationFilter</code> - whitelist sanitize user markup</li>
<li><code>SyntaxHighlightFilter</code> - code syntax highlighter</li>
<li><code>TextileFilter</code> - convert textile to html</li>
<li><code>TableOfContentsFilter</code> - anchor headings with name attributes and generate Table of Contents html unordered list linking headings</li>
</ul>
<h2 id="dependencies">Dependencies</h2>
<p>Filter gem dependencies are not bundled; you must bundle the filter’s gem dependencies. The below list details filters with dependencies. For example, <code>SyntaxHighlightFilter</code> uses <a href="https://github.com/jneen/rouge">rouge</a> to detect and highlight languages. For example, to use the <code>SyntaxHighlightFilter</code>, add the following to your Gemfile:</p>
<div class="sourceCode" id="cb9"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb9-1" title="1">gem <span class="st">&#39;rouge&#39;</span></a></code></pre></div>
<ul>
<li><code>AutolinkFilter</code> - <code>rinku</code></li>
<li><code>EmailReplyFilter</code> - <code>escape_utils</code>, <code>email_reply_parser</code></li>
<li><code>EmojiFilter</code> - <code>gemoji</code></li>
<li><code>MarkdownFilter</code> - <code>commonmarker</code></li>
<li><code>PlainTextInputFilter</code> - <code>escape_utils</code></li>
<li><code>SanitizationFilter</code> - <code>sanitize</code></li>
<li><code>SyntaxHighlightFilter</code> - <code>rouge</code></li>
<li><code>TableOfContentsFilter</code> - <code>escape_utils</code></li>
<li><code>TextileFilter</code> - <code>RedCloth</code></li>
</ul>
<p><em>Note:</em> See <a href="/Gemfile">Gemfile</a> <code>:test</code> block for version requirements.</p>
<h2 id="documentation">Documentation</h2>
<p>Full reference documentation can be <a href="http://rubydoc.info/gems/html-pipeline/frames">found here</a>.</p>
<h2 id="extending">Extending</h2>
<p>To write a custom filter, you need a class with a <code>call</code> method that inherits from <code>HTML::Pipeline::Filter</code>.</p>
<p>For example this filter adds a base url to images that are root relative:</p>
<div class="sourceCode" id="cb10"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb10-1" title="1">require <span class="st">&#39;uri&#39;</span></a>
<a class="sourceLine" id="cb10-2" title="2"></a>
<a class="sourceLine" id="cb10-3" title="3"><span class="kw">class</span> <span class="dt">RootRelativeFilter</span> &lt; <span class="dt">HTML</span>::<span class="dt">Pipeline</span>::<span class="dt">Filter</span></a>
<a class="sourceLine" id="cb10-4" title="4"></a>
<a class="sourceLine" id="cb10-5" title="5">  <span class="kw">def</span> call</a>
<a class="sourceLine" id="cb10-6" title="6">    doc.search(<span class="st">&quot;img&quot;</span>).each <span class="kw">do</span> |img|</a>
<a class="sourceLine" id="cb10-7" title="7">      <span class="kw">next</span> <span class="kw">if</span> img[<span class="st">&#39;src&#39;</span>].nil?</a>
<a class="sourceLine" id="cb10-8" title="8">      src = img[<span class="st">&#39;src&#39;</span>].strip</a>
<a class="sourceLine" id="cb10-9" title="9">      <span class="kw">if</span> src.start_with? <span class="ch">&#39;/&#39;</span></a>
<a class="sourceLine" id="cb10-10" title="10">        img[<span class="st">&quot;src&quot;</span>] = <span class="dt">URI</span>.join(context[<span class="st">:base_url</span>], src).to_s</a>
<a class="sourceLine" id="cb10-11" title="11">      <span class="kw">end</span></a>
<a class="sourceLine" id="cb10-12" title="12">    <span class="kw">end</span></a>
<a class="sourceLine" id="cb10-13" title="13">    doc</a>
<a class="sourceLine" id="cb10-14" title="14">  <span class="kw">end</span></a>
<a class="sourceLine" id="cb10-15" title="15"></a>
<a class="sourceLine" id="cb10-16" title="16"><span class="kw">end</span></a></code></pre></div>
<p>Now this filter can be used in a pipeline:</p>
<div class="sourceCode" id="cb11"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb11-1" title="1"><span class="dt">Pipeline</span>.new [ <span class="dt">RootRelativeFilter</span> ], { <span class="st">:base_url</span> =&gt; <span class="st">&#39;http://somehost.com&#39;</span> }</a></code></pre></div>
<h3 id="rd-party-extensions">3rd Party Extensions</h3>
<p>If you have an idea for a filter, propose it as <a href="https://github.com/jch/html-pipeline/issues">an issue</a> first. This allows us discuss whether the filter is a common enough use case to belong in this gem, or should be built as an external gem.</p>
<p>Here are some extensions people have built:</p>
<ul>
<li><a href="https://github.com/asciidoctor/html-pipeline-asciidoc_filter">html-pipeline-asciidoc_filter</a></li>
<li><a href="https://github.com/gjtorikian/jekyll-html-pipeline">jekyll-html-pipeline</a></li>
<li><a href="https://github.com/burnto/nanoc-html-pipeline">nanoc-html-pipeline</a></li>
<li><a href="https://github.com/dewski/html-pipeline-bitly">html-pipeline-bitly</a></li>
<li><a href="https://github.com/lifted-studios/html-pipeline-cite">html-pipeline-cite</a></li>
<li><a href="https://github.com/bradgessler/tilt-html-pipeline">tilt-html-pipeline</a></li>
<li><a href="https://github.com/lifted-studios/html-pipeline-wiki-link">html-pipeline-wiki-link’</a> - WikiMedia-style wiki links</li>
<li><a href="https://github.com/github/task_list">task_list</a> - GitHub flavor Markdown Task List</li>
<li><a href="https://github.com/rutan/html-pipeline-nico_link">html-pipeline-nico_link</a> - An HTML::Pipeline filter for <a href="http://www.nicovideo.jp">niconico</a> description links</li>
<li><a href="https://gitlab.com/gitlab-org/html-pipeline-gitlab">html-pipeline-gitlab</a> - This gem implements various filters for html-pipeline used by GitLab</li>
<li><a href="https://github.com/st0012/html-pipeline-youtube">html-pipeline-youtube</a> - An HTML::Pipeline filter for YouTube links</li>
<li><a href="https://github.com/st0012/html-pipeline-flickr">html-pipeline-flickr</a> - An HTML::Pipeline filter for Flickr links</li>
<li><a href="https://github.com/dlackty/html-pipeline-vimeo">html-pipeline-vimeo</a> - An HTML::Pipeline filter for Vimeo links</li>
<li><a href="https://github.com/mr-dxdy/html-pipeline-hashtag">html-pipeline-hashtag</a> - An HTML::Pipeline filter for hashtags</li>
<li><a href="https://github.com/jollygoodcode/html-pipeline-linkify_github">html-pipeline-linkify_github</a> - An HTML::Pipeline filter to autolink GitHub urls</li>
<li><a href="https://github.com/bmikol/html-pipeline-redcarpet_filter">html-pipeline-redcarpet_filter</a> - Render Markdown source text into Markdown HTML using Redcarpet</li>
<li><a href="https://github.com/bmikol/html-pipeline-typogruby_filter">html-pipeline-typogruby_filter</a> - Add Typogruby text filters to your HTML::Pipeline</li>
<li><a href="https://github.com/jodeci/korgi">korgi</a> - HTML::Pipeline filters for links to Rails resources</li>
</ul>
<h2 id="instrumenting">Instrumenting</h2>
<p>Filters and Pipelines can be set up to be instrumented when called. The pipeline must be setup with an <a href="http://api.rubyonrails.org/classes/ActiveSupport/Notifications.html">ActiveSupport::Notifications</a> compatible service object and a name. New pipeline objects will default to the <code>HTML::Pipeline.default_instrumentation_service</code> object.</p>
<div class="sourceCode" id="cb12"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb12-1" title="1"><span class="co"># the AS::Notifications-compatible service object</span></a>
<a class="sourceLine" id="cb12-2" title="2">service = <span class="dt">ActiveSupport</span>::<span class="dt">Notifications</span></a>
<a class="sourceLine" id="cb12-3" title="3"></a>
<a class="sourceLine" id="cb12-4" title="4"><span class="co"># instrument a specific pipeline</span></a>
<a class="sourceLine" id="cb12-5" title="5">pipeline = <span class="dt">HTML</span>::<span class="dt">Pipeline</span>.new [<span class="dt">MarkdownFilter</span>], context</a>
<a class="sourceLine" id="cb12-6" title="6">pipeline.setup_instrumentation <span class="st">&quot;MarkdownPipeline&quot;</span>, service</a>
<a class="sourceLine" id="cb12-7" title="7"></a>
<a class="sourceLine" id="cb12-8" title="8"><span class="co"># or set default instrumentation service for all new pipelines</span></a>
<a class="sourceLine" id="cb12-9" title="9"><span class="dt">HTML</span>::<span class="dt">Pipeline</span>.default_instrumentation_service = service</a>
<a class="sourceLine" id="cb12-10" title="10">pipeline = <span class="dt">HTML</span>::<span class="dt">Pipeline</span>.new [<span class="dt">MarkdownFilter</span>], context</a>
<a class="sourceLine" id="cb12-11" title="11">pipeline.setup_instrumentation <span class="st">&quot;MarkdownPipeline&quot;</span></a></code></pre></div>
<p>Filters are instrumented when they are run through the pipeline. A <code>call_filter.html_pipeline</code> event is published once the filter finishes. The <code>payload</code> should include the <code>filter</code> name. Each filter will trigger its own instrumentation call.</p>
<div class="sourceCode" id="cb13"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb13-1" title="1">service.subscribe <span class="st">&quot;call_filter.html_pipeline&quot;</span> <span class="kw">do</span> |event, start, ending, transaction_id, payload|</a>
<a class="sourceLine" id="cb13-2" title="2">  payload[<span class="st">:pipeline</span>] <span class="co">#=&gt; &quot;MarkdownPipeline&quot;, set with `setup_instrumentation`</span></a>
<a class="sourceLine" id="cb13-3" title="3">  payload[<span class="st">:filter</span>] <span class="co">#=&gt; &quot;MarkdownFilter&quot;</span></a>
<a class="sourceLine" id="cb13-4" title="4">  payload[<span class="st">:context</span>] <span class="co">#=&gt; context Hash</span></a>
<a class="sourceLine" id="cb13-5" title="5">  payload[<span class="st">:result</span>] <span class="co">#=&gt; instance of result class</span></a>
<a class="sourceLine" id="cb13-6" title="6">  payload[<span class="st">:result</span>][<span class="st">:output</span>] <span class="co">#=&gt; output HTML String or Nokogiri::DocumentFragment</span></a>
<a class="sourceLine" id="cb13-7" title="7"><span class="kw">end</span></a></code></pre></div>
<p>The full pipeline is also instrumented:</p>
<div class="sourceCode" id="cb14"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb14-1" title="1">service.subscribe <span class="st">&quot;call_pipeline.html_pipeline&quot;</span> <span class="kw">do</span> |event, start, ending, transaction_id, payload|</a>
<a class="sourceLine" id="cb14-2" title="2">  payload[<span class="st">:pipeline</span>] <span class="co">#=&gt; &quot;MarkdownPipeline&quot;, set with `setup_instrumentation`</span></a>
<a class="sourceLine" id="cb14-3" title="3">  payload[<span class="st">:filters</span>] <span class="co">#=&gt; [&quot;MarkdownFilter&quot;]</span></a>
<a class="sourceLine" id="cb14-4" title="4">  payload[<span class="st">:doc</span>] <span class="co">#=&gt; HTML String or Nokogiri::DocumentFragment</span></a>
<a class="sourceLine" id="cb14-5" title="5">  payload[<span class="st">:context</span>] <span class="co">#=&gt; context Hash</span></a>
<a class="sourceLine" id="cb14-6" title="6">  payload[<span class="st">:result</span>] <span class="co">#=&gt; instance of result class</span></a>
<a class="sourceLine" id="cb14-7" title="7">  payload[<span class="st">:result</span>][<span class="st">:output</span>] <span class="co">#=&gt; output HTML String or Nokogiri::DocumentFragment</span></a>
<a class="sourceLine" id="cb14-8" title="8"><span class="kw">end</span></a></code></pre></div>
<h2 id="faq">FAQ</h2>
<h3 id="why-doesnt-my-pipeline-work-when-theres-no-root-element-in-the-document">1. Why doesn’t my pipeline work when there’s no root element in the document?</h3>
<p>To make a pipeline work on a plain text document, put the <code>PlainTextInputFilter</code> at the beginning of your pipeline. This will wrap the content in a <code>div</code> so the filters have a root element to work with. If you’re passing in an HTML fragment, but it doesn’t have a root element, you can wrap the content in a <code>div</code> yourself. For example:</p>
<div class="sourceCode" id="cb15"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb15-1" title="1"><span class="dt">EmojiPipeline</span> = <span class="dt">Pipeline</span>.new [</a>
<a class="sourceLine" id="cb15-2" title="2">  <span class="dt">PlainTextInputFilter</span>,  <span class="co"># &lt;- Wraps input in a div and escapes html tags</span></a>
<a class="sourceLine" id="cb15-3" title="3">  <span class="dt">EmojiFilter</span></a>
<a class="sourceLine" id="cb15-4" title="4">], context</a>
<a class="sourceLine" id="cb15-5" title="5"></a>
<a class="sourceLine" id="cb15-6" title="6">plain_text = <span class="st">&quot;Gutentag! :wave:&quot;</span></a>
<a class="sourceLine" id="cb15-7" title="7"><span class="dt">EmojiPipeline</span>.call(plain_text)</a>
<a class="sourceLine" id="cb15-8" title="8"></a>
<a class="sourceLine" id="cb15-9" title="9">html_fragment = <span class="st">&quot;This is outside of an html element, but &lt;strong&gt;this isn&#39;t. :+1:&lt;/strong&gt;&quot;</span></a>
<a class="sourceLine" id="cb15-10" title="10"><span class="dt">EmojiPipeline</span>.call(<span class="st">&quot;&lt;div&gt;</span><span class="ot">#{</span>html_fragment<span class="ot">}</span><span class="st">&lt;/div&gt;&quot;</span>) <span class="co"># &lt;- Wrap your own html fragments to avoid escaping</span></a></code></pre></div>
<h3 id="how-do-i-customize-a-whitelist-for-sanitizationfilters">2. How do I customize a whitelist for <code>SanitizationFilter</code>s?</h3>
<p><code>SanitizationFilter::WHITELIST</code> is the default whitelist used if no <code>:whitelist</code> argument is given in the context. The default is a good starting template for you to add additional elements. You can either modify the constant’s value, or re-define your own constant and pass that in via the context.</p>
<h2 id="contributing">Contributing</h2>
<p>Please review the <a href="https://github.com/jch/html-pipeline/blob/master/CONTRIBUTING.md">Contributing Guide</a>.</p>
<ol type="1">
<li><a href="https://help.github.com/articles/fork-a-repo">Fork it</a></li>
<li>Create your feature branch (<code>git checkout -b my-new-feature</code>)</li>
<li>Commit your changes (<code>git commit -am 'Added some feature'</code>)</li>
<li>Push to the branch (<code>git push origin my-new-feature</code>)</li>
<li>Create new <a href="https://help.github.com/articles/using-pull-requests">Pull Request</a></li>
</ol>
<p>To see what has changed in recent versions, see the <a href="https://github.com/jch/html-pipeline/blob/master/CHANGELOG.md">CHANGELOG</a>.</p>
<h3 id="contributors">Contributors</h3>
<p>Thanks to all of <a href="https://github.com/jch/html-pipeline/graphs/contributors">these contributors</a>.</p>
<p>Project is a member of the <a href="http://ossmanifesto.org/">OSS Manifesto</a>.</p>
<h3 id="releasing-a-new-version">Releasing A New Version</h3>
<p>This section is for gem maintainers to cut a new version of the gem.</p>
<ul>
<li>create a new branch named <code>release-x.y.z</code> where <code>x.y.z</code> follows <a href="http://semver.org">semver</a></li>
<li>update lib/html/pipeline/version.rb to next version number X.X.X</li>
<li>update CHANGELOG.md. Prepare a draft with <code>script/changelog</code></li>
<li>push branch and create a new pull request</li>
<li>after tests are green, merge to master</li>
<li>on the master branch, run <code>script/release</code></li>
</ul>
