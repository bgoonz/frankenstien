<h1 id="minima">minima</h1>
<p><em>Minima is a one-size-fits-all Jekyll theme for writers</em>. It’s Jekyll’s default (and first) theme. It’s what you get when you run <code>jekyll new</code>.</p>
<p><a href="https://jekyll.github.io/minima/">Theme preview</a></p>
<figure>
<img src="/screenshot.png" alt="minima theme preview" /><figcaption>minima theme preview</figcaption>
</figure>
<h2 id="installation">Installation</h2>
<p>Add this line to your Jekyll site’s Gemfile:</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb1-1" title="1">gem <span class="st">&quot;minima&quot;</span></a></code></pre></div>
<p>And add this line to your Jekyll site:</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode yaml"><code class="sourceCode yaml"><a class="sourceLine" id="cb2-1" title="1"><span class="fu">theme:</span><span class="at"> minima</span></a></code></pre></div>
<p>And then execute:</p>
<pre><code>$ bundle</code></pre>
<h2 id="contents-at-a-glance">Contents At-A-Glance</h2>
<p>Minima has been scaffolded by the <code>jekyll new-theme</code> command and therefore has all the necessary files and directories to have a new Jekyll site up and running with zero-configuration.</p>
<h3 id="layouts">Layouts</h3>
<p>Refers to files within the <code>_layouts</code> directory, that define the markup for your theme.</p>
<ul>
<li><code>default.html</code> — The base layout that lays the foundation for subsequent layouts. The derived layouts inject their contents into this file at the line that says <code>{{ content }}</code> and are linked to this file via <a href="https://jekyllrb.com/docs/frontmatter/">FrontMatter</a> declaration <code>layout: default</code>.</li>
<li><code>home.html</code> — The layout for your landing-page / home-page / index-page. [<a href="#home-layout">More Info.</a>]</li>
<li><code>page.html</code> — The layout for your documents that contain FrontMatter, but are not posts.</li>
<li><code>post.html</code> — The layout for your posts.</li>
</ul>
<h3 id="includes">Includes</h3>
<p>Refers to snippets of code within the <code>_includes</code> directory that can be inserted in multiple layouts (and another include-file as well) within the same theme-gem.</p>
<ul>
<li><code>disqus_comments.html</code> — Code to markup disqus comment box.</li>
<li><code>footer.html</code> — Defines the site’s footer section.</li>
<li><code>google-analytics.html</code> — Inserts Google Analytics module (active only in production environment).</li>
<li><code>head.html</code> — Code-block that defines the <code>&lt;head&gt;&lt;/head&gt;</code> in <em>default</em> layout.</li>
<li><code>header.html</code> — Defines the site’s main header section. By default, pages with a defined <code>title</code> attribute will have links displayed here.</li>
</ul>
<h3 id="sass">Sass</h3>
<p>Refers to <code>.scss</code> files within the <code>_sass</code> directory that define the theme’s styles.</p>
<ul>
<li><code>minima.scss</code> — The core file imported by preprocessed <code>main.scss</code>, it defines the variable defaults for the theme and also further imports sass partials to supplement itself.</li>
<li><code>minima/_base.scss</code> — Resets and defines base styles for various HTML elements.</li>
<li><code>minima/_layout.scss</code> — Defines the visual style for various layouts.</li>
<li><code>minima/_syntax-highlighting.scss</code> — Defines the styles for syntax-highlighting.</li>
</ul>
<h3 id="assets">Assets</h3>
<p>Refers to various asset files within the <code>assets</code> directory. Contains the <code>main.scss</code> that imports sass files from within the <code>_sass</code> directory. This <code>main.scss</code> is what gets processed into the theme’s main stylesheet <code>main.css</code> called by <code>_layouts/default.html</code> via <code>_includes/head.html</code>.</p>
<p>This directory can include sub-directories to manage assets of similar type, and will be copied over as is, to the final transformed site directory.</p>
<h3 id="plugins">Plugins</h3>
<p>Minima comes with <a href="https://github.com/jekyll/jekyll-seo-tag"><code>jekyll-seo-tag</code></a> plugin preinstalled to make sure your website gets the most useful meta tags. See <a href="https://github.com/jekyll/jekyll-seo-tag#usage">usage</a> to know how to set it up.</p>
<h2 id="usage">Usage</h2>
<h3 id="home-layout">Home Layout</h3>
<p><code>home.html</code> is a flexible HTML layout for the site’s landing-page / home-page / index-page. <br/></p>
<h4 id="main-heading-and-content-injection">Main Heading and Content-injection</h4>
<p>From Minima v2.2 onwards, the <em>home</em> layout will inject all content from your <code>index.md</code> / <code>index.html</code> <strong>before</strong> the <strong><code>Posts</code></strong> heading. This will allow you to include non-posts related content to be published on the landing page under a dedicated heading. <em>We recommended that you title this section with a Heading2 (<code>##</code>)</em>.</p>
<p>Usually the <code>site.title</code> itself would suffice as the implicit ‘main-title’ for a landing-page. But, if your landing-page would like a heading to be explicitly displayed, then simply define a <code>title</code> variable in the document’s front matter and it will be rendered with an <code>&lt;h1&gt;</code> tag.</p>
<h4 id="post-listing">Post Listing</h4>
<p>This section is optional from Minima v2.2 onwards.<br/> It will be automatically included only when your site contains one or more valid posts or drafts (if the site is configured to <code>show_drafts</code>).</p>
<p>The title for this section is <code>Posts</code> by default and rendered with an <code>&lt;h2&gt;</code> tag. You can customize this heading by defining a <code>list_title</code> variable in the document’s front matter.</p>
<p>–</p>
<h3 id="customization">Customization</h3>
<p>To override the default structure and style of minima, simply create the concerned directory at the root of your site, copy the file you wish to customize to that directory, and then edit the file. e.g., to override the <a href="_includes/head.html"><code>_includes/head.html</code></a> file to specify a custom style path, create an <code>_includes</code> directory, copy <code>_includes/head.html</code> from minima gem folder to <code>&lt;yoursite&gt;/_includes</code> and start editing that file.</p>
<p>The site’s default CSS has now moved to a new place within the gem itself, <a href="assets/main.scss"><code>assets/main.scss</code></a>. To <strong>override the default CSS</strong>, the file has to exist at your site source. Do either of the following: - Create a new instance of <code>main.scss</code> at site source. - Create a new file <code>main.scss</code> at <code>&lt;your-site&gt;/assets/</code> - Add the frontmatter dashes, and - Add <code>@import "minima";</code>, to <code>&lt;your-site&gt;/assets/main.scss</code> - Add your custom CSS. - Download the file from this repo - Create a new file <code>main.scss</code> at <code>&lt;your-site&gt;/assets/</code> - Copy the contents at <a href="assets/main.scss">assets/main.scss</a> onto the <code>main.scss</code> you just created, and edit away! - Copy directly from Minima 2.0 gem - Go to your local minima gem installation directory ( run <code>bundle show minima</code> to get the path to it ). - Copy the <code>assets/</code> folder from there into the root of <code>&lt;your-site&gt;</code> - Change whatever values you want, inside <code>&lt;your-site&gt;/assets/main.scss</code></p>
<p>–</p>
<h3 id="customize-navigation-links">Customize navigation links</h3>
<p>This allows you to set which pages you want to appear in the navigation area and configure order of the links.</p>
<p>For instance, to only link to the <code>about</code> and the <code>portfolio</code> page, add the following to you <code>_config.yml</code>:</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode yaml"><code class="sourceCode yaml"><a class="sourceLine" id="cb4-1" title="1"><span class="fu">header_pages:</span></a>
<a class="sourceLine" id="cb4-2" title="2">  <span class="kw">-</span> about.md</a>
<a class="sourceLine" id="cb4-3" title="3">  <span class="kw">-</span> portfolio.md</a></code></pre></div>
<p>–</p>
<h3 id="change-default-date-format">Change default date format</h3>
<p>You can change the default date format by specifying <code>site.minima.date_format</code> in <code>_config.yml</code>.</p>
<pre><code># Minima date format
# refer to http://shopify.github.io/liquid/filters/date/ if you want to customize this
minima:
  date_format: &quot;%b %-d, %Y&quot;</code></pre>
<p>–</p>
<h3 id="enabling-comments-via-disqus">Enabling comments (via Disqus)</h3>
<p>Optionally, if you have a Disqus account, you can tell Jekyll to use it to show a comments section below each post.</p>
<p>To enable it, add the following lines to your Jekyll site:</p>
<div class="sourceCode" id="cb6"><pre class="sourceCode yaml"><code class="sourceCode yaml"><a class="sourceLine" id="cb6-1" title="1">  <span class="fu">disqus:</span></a>
<a class="sourceLine" id="cb6-2" title="2">    <span class="fu">shortname:</span><span class="at"> my_disqus_shortname</span></a></code></pre></div>
<p>You can find out more about Disqus’ shortnames <a href="https://help.disqus.com/customer/portal/articles/466208">here</a>.</p>
<p>Comments are enabled by default and will only appear in production, i.e., <code>JEKYLL_ENV=production</code></p>
<p>If you don’t want to display comments for a particular post you can disable them by adding <code>comments: false</code> to that post’s YAML Front Matter.</p>
<p>–</p>
<h3 id="social-networks">Social networks</h3>
<p>You can add links to the accounts you have on other sites, with respective icon, by adding one or more of the following options in your config:</p>
<div class="sourceCode" id="cb7"><pre class="sourceCode yaml"><code class="sourceCode yaml"><a class="sourceLine" id="cb7-1" title="1"><span class="fu">twitter_username:</span><span class="at"> jekyllrb</span></a>
<a class="sourceLine" id="cb7-2" title="2"><span class="fu">github_username:</span><span class="at">  jekyll</span></a>
<a class="sourceLine" id="cb7-3" title="3"><span class="fu">dribbble_username:</span><span class="at"> jekyll</span></a>
<a class="sourceLine" id="cb7-4" title="4"><span class="fu">facebook_username:</span><span class="at"> jekyll</span></a>
<a class="sourceLine" id="cb7-5" title="5"><span class="fu">flickr_username:</span><span class="at"> jekyll</span></a>
<a class="sourceLine" id="cb7-6" title="6"><span class="fu">instagram_username:</span><span class="at"> jekyll</span></a>
<a class="sourceLine" id="cb7-7" title="7"><span class="fu">linkedin_username:</span><span class="at"> jekyll</span></a>
<a class="sourceLine" id="cb7-8" title="8"><span class="fu">pinterest_username:</span><span class="at"> jekyll</span></a>
<a class="sourceLine" id="cb7-9" title="9"><span class="fu">youtube_username:</span><span class="at"> jekyll</span></a>
<a class="sourceLine" id="cb7-10" title="10"><span class="fu">googleplus_username:</span><span class="at"> +jekyll</span></a>
<a class="sourceLine" id="cb7-11" title="11"><span class="fu">rss:</span><span class="at"> rss</span></a>
<a class="sourceLine" id="cb7-12" title="12"></a>
<a class="sourceLine" id="cb7-13" title="13"><span class="fu">mastodon:</span></a>
<a class="sourceLine" id="cb7-14" title="14"> <span class="kw">-</span> <span class="fu">username:</span><span class="at"> jekyll</span></a>
<a class="sourceLine" id="cb7-15" title="15">   <span class="fu">instance:</span><span class="at"> example.com</span></a>
<a class="sourceLine" id="cb7-16" title="16"> <span class="kw">-</span> <span class="fu">username:</span><span class="at"> jekyll2</span></a>
<a class="sourceLine" id="cb7-17" title="17">   <span class="fu">instance:</span><span class="at"> example.com</span></a></code></pre></div>
<p>–</p>
<h3 id="enabling-google-analytics">Enabling Google Analytics</h3>
<p>To enable Google Analytics, add the following lines to your Jekyll site:</p>
<div class="sourceCode" id="cb8"><pre class="sourceCode yaml"><code class="sourceCode yaml"><a class="sourceLine" id="cb8-1" title="1">  <span class="fu">google_analytics:</span><span class="at"> UA-NNNNNNNN-N</span></a></code></pre></div>
<p>Google Analytics will only appear in production, i.e., <code>JEKYLL_ENV=production</code></p>
<p>–</p>
<h3 id="enabling-excerpts-on-the-home-page">Enabling Excerpts on the Home Page</h3>
<p>To display post-excerpts on the Home Page, simply add the following to your <code>_config.yml</code>:</p>
<div class="sourceCode" id="cb9"><pre class="sourceCode yaml"><code class="sourceCode yaml"><a class="sourceLine" id="cb9-1" title="1"><span class="fu">show_excerpts:</span><span class="at"> </span><span class="ch">true</span></a></code></pre></div>
<h2 id="contributing">Contributing</h2>
<p>Bug reports and pull requests are welcome on GitHub at https://github.com/jekyll/minima. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the <a href="http://contributor-covenant.org">Contributor Covenant</a> code of conduct.</p>
<h2 id="development">Development</h2>
<p>To set up your environment to develop this theme, run <code>script/bootstrap</code>.</p>
<p>To test your theme, run <code>script/server</code> (or <code>bundle exec jekyll serve</code>) and open your browser at <code>http://localhost:4000</code>. This starts a Jekyll server using your theme and the contents. As you make modifications, your site will regenerate and you should see the changes in the browser after a refresh.</p>
<h2 id="license">License</h2>
<p>The theme is available as open source under the terms of the <a href="http://opensource.org/licenses/MIT">MIT License</a>.</p>
