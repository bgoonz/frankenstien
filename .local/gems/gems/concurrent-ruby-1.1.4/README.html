<h1 id="concurrent-ruby">Concurrent Ruby</h1>
<p><a href="http://badge.fury.io/rb/concurrent-ruby"><img src="https://badge.fury.io/rb/concurrent-ruby.svg" alt="Gem Version" /></a> <a href="https://travis-ci.org/ruby-concurrency/concurrent-ruby"><img src="https://travis-ci.org/ruby-concurrency/concurrent-ruby.svg?branch=master" alt="Build Status" /></a> <a href="https://ci.appveyor.com/project/rubyconcurrency/concurrent-ruby"><img src="https://ci.appveyor.com/api/projects/status/iq8aboyuu3etad4w?svg=true" alt="Build status" /></a> <a href="http://opensource.org/licenses/MIT"><img src="https://img.shields.io/badge/license-MIT-green.svg" alt="License" /></a> <a href="https://gitter.im/ruby-concurrency/concurrent-ruby"><img src="https://img.shields.io/badge/IRC%20(gitter)-devs%20%26%20users-brightgreen.svg" alt="Gitter chat" /></a></p>
<p>Modern concurrency tools for Ruby. Inspired by <a href="http://www.erlang.org/doc/reference_manual/processes.html">Erlang</a>, <a href="http://clojure.org/concurrent_programming">Clojure</a>, <a href="http://akka.io/">Scala</a>, <a href="http://www.haskell.org/haskellwiki/Applications_and_libraries/Concurrency_and_parallelism#Concurrent_Haskell">Haskell</a>, <a href="http://blogs.msdn.com/b/dsyme/archive/2010/02/15/async-and-parallel-design-patterns-in-f-part-3-agents.aspx">F#</a>, <a href="http://msdn.microsoft.com/en-us/library/vstudio/hh191443.aspx">C#</a>, <a href="http://docs.oracle.com/javase/7/docs/api/java/util/concurrent/package-summary.html">Java</a>, and classic concurrency patterns.</p>
<p><img src="https://raw.githubusercontent.com/ruby-concurrency/concurrent-ruby/master/docs-source/logo/concurrent-ruby-logo-300x300.png" align="right" style="margin-left: 20px;" /></p>
<p>The design goals of this gem are:</p>
<ul>
<li>Be an ‘unopinionated’ toolbox that provides useful utilities without debating which is better or why</li>
<li>Remain free of external gem dependencies</li>
<li>Stay true to the spirit of the languages providing inspiration</li>
<li>But implement in a way that makes sense for Ruby</li>
<li>Keep the semantics as idiomatic Ruby as possible</li>
<li>Support features that make sense in Ruby</li>
<li>Exclude features that don’t make sense in Ruby</li>
<li>Be small, lean, and loosely coupled</li>
<li>Thread-safety</li>
<li>Backward compatibility</li>
</ul>
<h2 id="contributing">Contributing</h2>
<p><strong>This gem depends on <a href="https://github.com/ruby-concurrency/concurrent-ruby/graphs/contributors">contributions</a> and we appreciate your help. Would you like to contribute? Great! Have a look at <a href="https://github.com/ruby-concurrency/concurrent-ruby/issues?q=is%3Aissue+is%3Aopen+label%3Alooking-for-contributor">issues with <code>looking-for-contributor</code> label</a>.</strong> And if you pick something up let us know on the issue.</p>
<h2 id="thread-safety">Thread Safety</h2>
<p><em>Concurrent Ruby makes one of the strongest thread safety guarantees of any Ruby concurrency library, providing consistent behavior and guarantees on all three of the main Ruby interpreters (MRI/CRuby, JRuby, Rubinius, TruffleRuby).</em></p>
<p>Every abstraction in this library is thread safe. Specific thread safety guarantees are documented with each abstraction.</p>
<p>It is critical to remember, however, that Ruby is a language of mutable references. <em>No</em> concurrency library for Ruby can ever prevent the user from making thread safety mistakes (such as sharing a mutable object between threads and modifying it on both threads) or from creating deadlocks through incorrect use of locks. All the library can do is provide safe abstractions which encourage safe practices. Concurrent Ruby provides more safe concurrency abstractions than any other Ruby library, many of which support the mantra of <a href="https://blog.golang.org/share-memory-by-communicating">“Do not communicate by sharing memory; instead, share memory by communicating”</a>. Concurrent Ruby is also the only Ruby library which provides a full suite of thread safe and immutable variable types and data structures.</p>
<p>We’ve also initiated discussion to document <a href="docs-source/synchronization.md">memory model</a> of Ruby which would provide consistent behaviour and guarantees on all four of the main Ruby interpreters (MRI/CRuby, JRuby, Rubinius, TruffleRuby).</p>
<h2 id="features-documentation">Features &amp; Documentation</h2>
<p><strong>The primary site for documentation is the automatically generated <a href="http://ruby-concurrency.github.io/concurrent-ruby/index.html">API documentation</a> which is up to date with latest release.</strong> This readme matches the master so may contain new stuff not yet released.</p>
<p>We also have a <a href="https://gitter.im/ruby-concurrency/concurrent-ruby">IRC (gitter)</a>.</p>
<h3 id="versioning">Versioning</h3>
<ul>
<li><code>concurrent-ruby</code> uses <a href="http://semver.org/">Semantic Versioning</a></li>
<li><code>concurrent-ruby-ext</code> has always same version as <code>concurrent-ruby</code></li>
<li><code>concurrent-ruby-edge</code> will always be 0.y.z therefore following <a href="http://semver.org/#spec-item-4">point 4</a> applies <em>“Major version zero (0.y.z) is for initial development. Anything may change at any time. The public API should not be considered stable.”</em> However we additionally use following rules:
<ul>
<li>Minor version increment means incompatible changes were made</li>
<li>Patch version increment means only compatible changes were made</li>
</ul></li>
</ul>
<h4 id="general-purpose-concurrency-abstractions">General-purpose Concurrency Abstractions</h4>
<ul>
<li><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/Async.html">Async</a>: A mixin module that provides simple asynchronous behavior to a class. Loosely based on Erlang’s <a href="http://www.erlang.org/doc/man/gen_server.html">gen_server</a>.</li>
<li><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/ScheduledTask.html">ScheduledTask</a>: Like a Future scheduled for a specific future time.</li>
<li><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/TimerTask.html">TimerTask</a>: A Thread that periodically wakes up to perform work at regular intervals.</li>
<li><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/Promises.html">Promises</a>: Unified implementation of futures and promises which combines features of previous <code>Future</code>, <code>Promise</code>, <code>IVar</code>, <code>Event</code>, <code>dataflow</code>, <code>Delay</code>, and (partially) <code>TimerTask</code> into a single framework. It extensively uses the new synchronization layer to make all the features <strong>non-blocking</strong> and <strong>lock-free</strong>, with the exception of obviously blocking operations like <code>#wait</code>, <code>#value</code>. It also offers better performance.</li>
</ul>
<h4 id="thread-safe-value-objects-structures-and-collections">Thread-safe Value Objects, Structures, and Collections</h4>
<p>Collection classes that were originally part of the (deprecated) <code>thread_safe</code> gem:</p>
<ul>
<li><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/Array.html">Array</a> A thread-safe subclass of Ruby’s standard <a href="http://ruby-doc.org/core-2.2.0/Array.html">Array</a>.</li>
<li><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/Hash.html">Hash</a> A thread-safe subclass of Ruby’s standard <a href="http://ruby-doc.org/core-2.2.0/Hash.html">Hash</a>.</li>
<li><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/Set.html">Set</a> A thread-safe subclass of Ruby’s standard <a href="http://ruby-doc.org/stdlib-2.4.0/libdoc/set/rdoc/Set.html">Set</a>.</li>
<li><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/Map.html">Map</a> A hash-like object that should have much better performance characteristics, especially under high concurrency, than <code>Concurrent::Hash</code>.</li>
<li><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/Tuple.html">Tuple</a> A fixed size array with volatile (synchronized, thread safe) getters/setters.</li>
</ul>
<p>Value objects inspired by other languages:</p>
<ul>
<li><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/Maybe.html">Maybe</a> A thread-safe, immutable object representing an optional value, based on <a href="https://hackage.haskell.org/package/base-4.2.0.1/docs/Data-Maybe.html">Haskell Data.Maybe</a>.</li>
</ul>
<p>Structure classes derived from Ruby’s <a href="http://ruby-doc.org/core-2.2.0/Struct.html">Struct</a>:</p>
<ul>
<li><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/ImmutableStruct.html">ImmutableStruct</a> Immutable struct where values are set at construction and cannot be changed later.</li>
<li><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/MutableStruct.html">MutableStruct</a> Synchronized, mutable struct where values can be safely changed at any time.</li>
<li><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/SettableStruct.html">SettableStruct</a> Synchronized, write-once struct where values can be set at most once, either at construction or any time thereafter.</li>
</ul>
<p>Thread-safe variables:</p>
<ul>
<li><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/Agent.html">Agent</a>: A way to manage shared, mutable, <em>asynchronous</em>, independent state. Based on Clojure’s <a href="http://clojure.org/agents">Agent</a>.</li>
<li><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/Atom.html">Atom</a>: A way to manage shared, mutable, <em>synchronous</em>, independent state. Based on Clojure’s <a href="http://clojure.org/atoms">Atom</a>.</li>
<li><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/AtomicBoolean.html">AtomicBoolean</a> A boolean value that can be updated atomically.</li>
<li><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/AtomicFixnum.html">AtomicFixnum</a> A numeric value that can be updated atomically.</li>
<li><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/AtomicReference.html">AtomicReference</a> An object reference that may be updated atomically.</li>
<li><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/Exchanger.html">Exchanger</a> A synchronization point at which threads can pair and swap elements within pairs. Based on Java’s <a href="http://docs.oracle.com/javase/7/docs/api/java/util/concurrent/Exchanger.html">Exchanger</a>.</li>
<li><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/MVar.html">MVar</a> A synchronized single element container. Based on Haskell’s <a href="https://hackage.haskell.org/package/base-4.8.1.0/docs/Control-Concurrent-MVar.html">MVar</a> and Scala’s <a href="http://docs.typelevel.org/api/scalaz/nightly/index.html#scalaz.concurrent.MVar$">MVar</a>.</li>
<li><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/ThreadLocalVar.html">ThreadLocalVar</a> A variable where the value is different for each thread.</li>
<li><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/TVar.html">TVar</a> A transactional variable implementing software transactional memory (STM). Based on Clojure’s <a href="http://clojure.org/refs">Ref</a>.</li>
</ul>
<h4 id="java-inspired-threadpools-and-other-executors">Java-inspired ThreadPools and Other Executors</h4>
<ul>
<li>See the <a href="http://ruby-concurrency.github.io/concurrent-ruby/master/file.thread_pools.html">thread pool</a> overview, which also contains a list of other Executors available.</li>
</ul>
<h4 id="thread-synchronization-classes-and-algorithms">Thread Synchronization Classes and Algorithms</h4>
<ul>
<li><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/CountDownLatch.html">CountDownLatch</a> A synchronization object that allows one thread to wait on multiple other threads.</li>
<li><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/CyclicBarrier.html">CyclicBarrier</a> A synchronization aid that allows a set of threads to all wait for each other to reach a common barrier point.</li>
<li><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/Event.html">Event</a> Old school kernel-style event.</li>
<li><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/ReadWriteLock.html">ReadWriteLock</a> A lock that supports multiple readers but only one writer.</li>
<li><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/ReentrantReadWriteLock.html">ReentrantReadWriteLock</a> A read/write lock with reentrant and upgrade features.</li>
<li><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/Semaphore.html">Semaphore</a> A counting-based locking mechanism that uses permits.</li>
<li><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/AtomicMarkableReference.html">AtomicMarkableReference</a></li>
</ul>
<h4 id="deprecated">Deprecated</h4>
<p>Deprecated features are still available and bugs are being fixed, but new features will not be added.</p>
<ul>
<li><del><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/Future.html">Future</a>: An asynchronous operation that produces a value.</del> Replaced by <a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/Promises.html">Promises</a>.
<ul>
<li><del><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent.html#dataflow-class_method">.dataflow</a>: Built on Futures, Dataflow allows you to create a task that will be scheduled when all of its data dependencies are available.</del> Replaced by <a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/Promises.html">Promises</a>.</li>
</ul></li>
<li><del><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/Promise.html">Promise</a>: Similar to Futures, with more features.</del> Replaced by <a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/Promises.html">Promises</a>.</li>
<li><del><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/Delay.html">Delay</a> Lazy evaluation of a block yielding an immutable result. Based on Clojure’s <a href="https://clojuredocs.org/clojure.core/delay">delay</a>.</del> Replaced by <a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/Promises.html">Promises</a>.</li>
<li><del><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/IVar.html">IVar</a> Similar to a “future” but can be manually assigned once, after which it becomes immutable.</del> Replaced by <a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/Promises.html">Promises</a>.</li>
</ul>
<h3 id="edge-features">Edge Features</h3>
<p>These are available in the <code>concurrent-ruby-edge</code> companion gem.</p>
<p>These features are under active development and may change frequently. They are expected not to keep backward compatibility (there may also lack tests and documentation). Semantic versions will be obeyed though. Features developed in <code>concurrent-ruby-edge</code> are expected to move to <code>concurrent-ruby</code> when final.</p>
<ul>
<li><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/Actor.html">Actor</a>: Implements the Actor Model, where concurrent actors exchange messages. <em>Status: Partial documentation and tests; depends on new future/promise framework; stability is good.</em></li>
<li><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/Channel.html">Channel</a>: Communicating Sequential Processes (<a href="https://en.wikipedia.org/wiki/Communicating_sequential_processes">CSP</a>). Functionally equivalent to Go <a href="https://tour.golang.org/concurrency/2">channels</a> with additional inspiration from Clojure <a href="https://clojure.github.io/core.async/">core.async</a>. <em>Status: Partial documentation and tests.</em></li>
<li><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/LazyRegister.html">LazyRegister</a></li>
<li><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/Edge/LockFreeLinkedSet.html">LockFreeLinkedSet</a> <em>Status: will be moved to core soon.</em></li>
<li><a href="http://ruby-concurrency.github.io/concurrent-ruby/master/Concurrent/LockFreeStack.html">LockFreeStack</a> <em>Status: missing documentation and tests.</em></li>
</ul>
<h2 id="supported-ruby-versions">Supported Ruby versions</h2>
<ul>
<li>MRI 2.0 and above</li>
<li>JRuby 9000</li>
<li>TruffleRuby are supported.</li>
<li>Any Ruby interpreter that is compliant with Ruby 2.0 or newer.</li>
</ul>
<p>Actually we still support mri 1.9.3 and jruby 1.7.27 but we are looking at ways how to drop the support. Java 8 is preferred for JRuby but every Java version on which JRuby 9000 runs is supported.</p>
<p>The legacy support for Rubinius is kept but it is no longer maintained, if you would like to help please respond to <a href="https://github.com/ruby-concurrency/concurrent-ruby/issues/739">#739</a>.</p>
<h2 id="usage">Usage</h2>
<p>Everything within this gem can be loaded simply by requiring it:</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb1-1" title="1">require <span class="st">&#39;concurrent&#39;</span></a></code></pre></div>
<p><em>Requiring only specific abstractions from Concurrent Ruby is not yet supported.</em></p>
<p>To use the tools in the Edge gem it must be required separately:</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb2-1" title="1">require <span class="st">&#39;concurrent-edge&#39;</span></a></code></pre></div>
<p>If the library does not behave as expected, <code>Concurrent.use_stdlib_logger(Logger::DEBUG)</code> could help to reveal the problem.</p>
<h2 id="installation">Installation</h2>
<pre class="shell"><code>gem install concurrent-ruby</code></pre>
<p>or add the following line to Gemfile:</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb4-1" title="1">gem <span class="st">&#39;concurrent-ruby&#39;</span>, require: <span class="st">&#39;concurrent&#39;</span></a></code></pre></div>
<p>and run <code>bundle install</code> from your shell.</p>
<h3 id="edge-gem-installation">Edge Gem Installation</h3>
<p>The Edge gem must be installed separately from the core gem:</p>
<pre class="shell"><code>gem install concurrent-ruby-edge</code></pre>
<p>or add the following line to Gemfile:</p>
<div class="sourceCode" id="cb6"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb6-1" title="1">gem <span class="st">&#39;concurrent-ruby-edge&#39;</span>, require: <span class="st">&#39;concurrent-edge&#39;</span></a></code></pre></div>
<p>and run <code>bundle install</code> from your shell.</p>
<h3 id="c-extensions-for-mri">C Extensions for MRI</h3>
<p>Potential performance improvements may be achieved under MRI by installing optional C extensions. To minimise installation errors the C extensions are available in the <code>concurrent-ruby-ext</code> extension gem. <code>concurrent-ruby</code> and <code>concurrent-ruby-ext</code> are always released together with same version. Simply install the extension gem too:</p>
<div class="sourceCode" id="cb7"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb7-1" title="1">gem install concurrent-ruby-ext</a></code></pre></div>
<p>or add the following line to Gemfile:</p>
<div class="sourceCode" id="cb8"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb8-1" title="1">gem <span class="st">&#39;concurrent-ruby-ext&#39;</span></a></code></pre></div>
<p>and run <code>bundle install</code> from your shell.</p>
<p>In code it is only necessary to</p>
<div class="sourceCode" id="cb9"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb9-1" title="1">require <span class="st">&#39;concurrent&#39;</span></a></code></pre></div>
<p>The <code>concurrent-ruby</code> gem will automatically detect the presence of the <code>concurrent-ruby-ext</code> gem and load the appropriate C extensions.</p>
<h4 id="note-for-gem-developers">Note For gem developers</h4>
<p>No gems should depend on <code>concurrent-ruby-ext</code>. Doing so will force C extensions on your users. The best practice is to depend on <code>concurrent-ruby</code> and let users to decide if they want C extensions.</p>
<h2 id="maintainers">Maintainers</h2>
<ul>
<li><a href="https://github.com/pitr-ch">Petr Chalupa</a> (lead maintainer, point-of-contact)</li>
<li><a href="https://github.com/jdantonio">Jerry D’Antonio</a> (creator)</li>
<li><a href="https://github.com/chrisseaton">Chris Seaton</a></li>
</ul>
<h3 id="special-thanks-to">Special Thanks to</h3>
<ul>
<li><a href="https://github.com/bdurand">Brian Durand</a> for the <code>ref</code> gem</li>
<li><a href="https://github.com/headius">Charles Oliver Nutter</a> for the <code>atomic</code> and <code>thread_safe</code> gems</li>
<li><a href="https://github.com/thedarkone">thedarkone</a> for the <code>thread_safe</code> gem</li>
</ul>
<p>and to the past maintainers</p>
<ul>
<li><a href="https://github.com/mighe">Michele Della Torre</a></li>
<li><a href="https://github.com/obrok">Paweł Obrok</a></li>
<li><a href="https://github.com/lucasallan">Lucas Allan</a></li>
</ul>
<h2 id="license-and-copyright">License and Copyright</h2>
<p><em>Concurrent Ruby</em> is free software released under the <a href="http://www.opensource.org/licenses/MIT">MIT License</a>.</p>
<p>The <em>Concurrent Ruby</em> <a href="https://github.com/ruby-concurrency/concurrent-ruby/wiki/Logo">logo</a> was designed by <a href="https://twitter.com/zombyboy">David Jones</a>. It is Copyright © 2014 <a href="https://twitter.com/jerrydantonio">Jerry D’Antonio</a>. All Rights Reserved.</p>
