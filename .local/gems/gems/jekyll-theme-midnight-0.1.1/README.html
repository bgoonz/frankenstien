<h1 id="the-midnight-theme">The Midnight theme</h1>
<p><a href="https://travis-ci.org/pages-themes/midnight"><img src="https://travis-ci.org/pages-themes/midnight.svg?branch=master" alt="Build Status" /></a> <a href="https://badge.fury.io/rb/jekyll-theme-midnight"><img src="https://badge.fury.io/rb/jekyll-theme-midnight.svg" alt="Gem Version" /></a></p>
<p><em>Midnight is a Jekyll theme for GitHub Pages. You can <a href="http://pages-themes.github.io/midnight">preview the theme to see what it looks like</a>, or even <a href="#usage">use it today</a>.</em></p>
<figure>
<img src="thumbnail.png" alt="Thumbnail of Midnight" /><figcaption>Thumbnail of Midnight</figcaption>
</figure>
<h2 id="usage">Usage</h2>
<p>To use the Midnight theme:</p>
<ol type="1">
<li><p>Add the following to your site’s <code>_config.yml</code>:</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode yml"><code class="sourceCode yaml"><a class="sourceLine" id="cb1-1" title="1"><span class="fu">theme:</span><span class="at"> jekyll-theme-midnight</span></a></code></pre></div></li>
<li><p>Optionally, if you’d like to preview your site on your computer, add the following to your site’s <code>Gemfile</code>:</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode ruby"><code class="sourceCode ruby"><a class="sourceLine" id="cb2-1" title="1">gem <span class="st">&quot;github-pages&quot;</span>, <span class="st">group: :jekyll_plugins</span></a></code></pre></div></li>
</ol>
<h2 id="customizing">Customizing</h2>
<h3 id="configuration-variables">Configuration variables</h3>
<p>Midnight will respect the following variables, if set in your site’s <code>_config.yml</code>:</p>
<div class="sourceCode" id="cb3"><pre class="sourceCode yml"><code class="sourceCode yaml"><a class="sourceLine" id="cb3-1" title="1"><span class="fu">title:</span><span class="at"> </span><span class="kw">[</span>The title of your site<span class="kw">]</span></a>
<a class="sourceLine" id="cb3-2" title="2"><span class="fu">description:</span><span class="at"> </span><span class="kw">[</span>A short description of your site<span class="st">&#39;s purpose]</span></a></code></pre></div>
<p>Additionally, you may choose to set the following optional variables:</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode yml"><code class="sourceCode yaml"><a class="sourceLine" id="cb4-1" title="1"><span class="fu">show_downloads:</span><span class="at"> </span><span class="kw">[</span><span class="st">&quot;true&quot;</span> or <span class="st">&quot;false&quot;</span> to indicate whether to provide a download URL<span class="kw">]</span></a>
<a class="sourceLine" id="cb4-2" title="2"><span class="fu">google_analytics:</span><span class="at"> </span><span class="kw">[</span>Your Google Analytics tracking ID<span class="kw">]</span></a></code></pre></div>
<h3 id="stylesheet">Stylesheet</h3>
<p>If you’d like to add your own custom styles:</p>
<ol type="1">
<li>Create a file called <code>/assets/css/style.scss</code> in your site</li>
<li><p>Add the following content to the top of the file, exactly as shown: ```scss — —</p>
<span class="citation" data-cites="import">@import</span> “{{ site.theme }}”; ```</li>
<li><p>Add any custom CSS (or Sass, including imports) you’d like immediately after the <code>@import</code> line</p></li>
</ol>
<p><em>Note: If you’d like to change the theme’s Sass variables, you must set new values before the <code>@import</code> line in your stylesheet.</em></p>
<h3 id="layouts">Layouts</h3>
<p>If you’d like to change the theme’s HTML layout:</p>
<ol type="1">
<li><a href="https://github.com/pages-themes/midnight/blob/master/_layouts/default.html">Copy the original template</a> from the theme’s repository<br />(<em>Pro-tip: click “raw” to make copying easier</em>)</li>
<li>Create a file called <code>/_layouts/default.html</code> in your site</li>
<li>Paste the default layout content copied in the first step</li>
<li>Customize the layout as you’d like</li>
</ol>
<h3 id="overriding-github-generated-urls">Overriding GitHub-generated URLs</h3>
<p>Templates often rely on URLs supplied by GitHub such as links to your repository or links to download your project. If you’d like to override one or more default URLs:</p>
<ol type="1">
<li>Look at <a href="https://github.com/pages-themes/midnight/blob/master/_layouts/default.html">the template source</a> to determine the name of the variable. It will be in the form of <code>{{ site.github.zip_url }}</code>.</li>
<li>Specify the URL that you’d like the template to use in your site’s <code>_config.yml</code>. For example, if the variable was <code>site.github.url</code>, you’d add the following: <code>yml  github:    zip_url: http://example.com/download.zip    another_url: another value</code></li>
<li>When your site is built, Jekyll will use the URL you specified, rather than the default one provided by GitHub.</li>
</ol>
<p><em>Note: You must remove the <code>site.</code> prefix, and each variable name (after the <code>github.</code>) should be indent with two space below <code>github:</code>.</em></p>
<p>For more information, see <a href="https://jekyllrb.com/docs/variables/">the Jekyll variables documentation</a>.</p>
<h2 id="roadmap">Roadmap</h2>
<p>See the <a href="https://github.com/pages-themes/midnight/issues">open issues</a> for a list of proposed features (and known issues).</p>
<h2 id="project-philosophy">Project philosophy</h2>
<p>The Midnight theme is intended to make it quick and easy for GitHub Pages users to create their first (or 100th) website. The theme should meet the vast majority of users’ needs out of the box, erring on the side of simplicity rather than flexibility, and provide users the opportunity to opt-in to additional complexity if they have specific needs or wish to further customize their experience (such as adding custom CSS or modifying the default layout). It should also look great, but that goes without saying.</p>
<h2 id="contributing">Contributing</h2>
<p>Interested in contributing to Midnight? We’d love your help. Midnight is an open source project, built one contribution at a time by users like you. See <a href="docs/CONTRIBUTING.md">the CONTRIBUTING file</a> for instructions on how to contribute.</p>
<h3 id="previewing-the-theme-locally">Previewing the theme locally</h3>
<p>If you’d like to preview the theme locally (for example, in the process of proposing a change):</p>
<ol type="1">
<li>Clone down the theme’s repository (<code>git clone https://github.com/pages-themes/midnight</code>)</li>
<li><code>cd</code> into the theme’s directory</li>
<li>Run <code>script/bootstrap</code> to install the necessary dependencies</li>
<li>Run <code>bundle exec jekyll serve</code> to start the preview server</li>
<li>Visit <a href="http://localhost:4000"><code>localhost:4000</code></a> in your browser to preview the theme</li>
</ol>
<h3 id="running-tests">Running tests</h3>
<p>The theme contains a minimal test suite, to ensure a site with the theme would build successfully. To run the tests, simply run <code>script/cibuild</code>. You’ll need to run <code>script/bootstrap</code> one before the test script will work.</p>
