  <!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html">
  <meta name="Author" content="Bryan Guner">

  <title> directory </title>

<style>
    a {
      color: black;
    }

    li {
      border: 1px solid black !important;
      letter-spacing: 0px;
      line-height: 16px;
      text-decoration: none !important;
      text-transform: uppercase;
      background: #194ccdaf !important;
      color: black !important;
      border: none;
      cursor: pointer;
      justify-content: center;
      padding: 30px 60px;
      height: 48px;
      text-align: center;
      white-space: normal;
      border-radius: 10px;
      min-width: 45em;
      padding: 1.2em 1em 0;
      box-shadow: 0 0 5px;
      margin: 1em;
      display: grid;
      -webkit-border-radius: 10px;
      -moz-border-radius: 10px;
      -ms-border-radius: 10px;
      -o-border-radius: 10px;
    }
  </style>
</head>
<body>

<div>
  <!-- Copy and Paste Me -->
  <div class="glitch-embed-wrap" style="height: 420px; width: 100%;">
    <iframe src="https://glitch.com/embed/#!/embed/offline-with-serviceworker?path=README.md&previewSize=100"
      title="offline-with-serviceworker on Glitch" allow="geolocation; microphone; camera; midi; vr; encrypted-media"
      style="height: 100%; width: 100%; border: 0;">
    </iframe>
  </div>
<hr>
  <div>
    <!-- Copy and Paste Me -->
    <div class="glitch-embed-wrap" style="height: 420px; width: 100%;">
      <iframe src="https://glitch.com/embed/#!/embed/link-tree-11ty?path=README.md&previewSize=100"
        title="link-tree-11ty on Glitch" allow="geolocation; microphone; camera; midi; vr; encrypted-media"
        style="height: 100%; width: 100%; border: 0;">
      </iframe>
    </div>
    
  </div>
  <hr>
  <div>
    <!-- Copy and Paste Me -->
    <div class="glitch-embed-wrap" style="height: 420px; width: 100%;">
      <iframe src="https://glitch.com/embed/#!/embed/geoglitchies?path=README.md&previewSize=100"
        title="geoglitchies on Glitch" allow="geolocation; microphone; camera; midi; vr; encrypted-media"
        style="height: 100%; width: 100%; border: 0;">
      </iframe>
    </div>
    
  </div>
  <hr>
  
  
    <div>
      <!-- Copy and Paste Me -->
      <div class="glitch-embed-wrap" style="height: 420px; width: 100%;">
        <iframe src="https://glitch.com/embed/#!/embed/shaded-bubble-operation?path=README.md&previewSize=100"
          title="shaded-bubble-operation on Glitch" allow="geolocation; microphone; camera; midi; vr; encrypted-media"
          style="height: 100%; width: 100%; border: 0;">
        </iframe>
      </div>
      
    </div>
<hr>
    <div>
      
    </div>
    <hr>
  
  
  
  
  
<ul>

